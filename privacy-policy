Welcome to ShopCi pro, the administration interface of the shopci multi-seller e-commerce platform. This Privacy Policy is intended to inform you about how we collect, use, disclose and protect your information when you use our site.

By using ShopCi pro, you consent to the collection and use of your information in accordance with this policy.

1. Information Collected

1.1 Registration Information: When you create an account on ShopCi pro, we collect information such as your name, your email address, and the information necessary for your registration.

1.2 Profile Information: You have the option to provide additional information about your profile, such as your shipping address and communication preferences.

1.3 Transaction Data: We collect information about your transactions, including products purchased, amounts charged and transaction dates.

2. Use of Information

2.1 We use your information to provide, maintain and improve our platform, as well as to facilitate transactions between buyers and sellers.

2.2 We may use your information to contact you about your account, transactions, platform updates or for promotional purposes.

3. Disclosure of Information

3.1 We do not sell, rent or share your personal information with third parties for their marketing purposes unless we obtain your consent or if this is required by law.

3.2 Your information may be shared with sellers on ShopsCi in connection with transactions and delivery of products.

3.3 We may disclose your information in response to legal requests or to protect our rights or safety or that of others.

4. Information Security

4.1 We implement physical, electronic and administrative security measures to protect your information against unauthorized access, alteration, disclosure or destruction.

4.2 Despite our efforts, no method of transmission over the Internet, or method of electronic storage, is completely secure. We cannot therefore guarantee absolute security of your information.

5. Cookies and Similar Technologies

5.1 ShopCi pro uses cookies and other similar technologies to improve user experience, understand preferences and analyze usage trends.

5.2 You can configure your browser to refuse cookies, but this may limit certain functionalities of ShopCi pro.

6. Changes to the Privacy Policy

6.1 We reserve the right to modify this policy at any time. Changes will be posted on ShopsCi, and the last update date will be changed accordingly.

6.2 We encourage you to regularly review this policy to stay informed about the collection and use practices of your information.

By using ShopsCi, you agree to this Privacy Policy. If you have any questions, do not hesitate to contact us at contact@shops-ci.com